package com.company;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{

    private StudentRepository repository;

    @Autowired
    public void setRepository(StudentRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Student> getStudentById(Integer id) {
        return repository.findById(id);
    }


    @Override
    public void saveStudent(Student student) {
        repository.save(student);
    }

}
