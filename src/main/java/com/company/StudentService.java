package com.company;

import java.util.Optional;

public interface StudentService {
    Optional<Student> getStudentById(Integer id);
    void saveStudent(Student student);
}

