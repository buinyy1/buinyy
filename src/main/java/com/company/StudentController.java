package com.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/student")
public class StudentController {

    private StudentService service;
    @Autowired
    public void setService(StudentService service) {
        this.service = service;
    }
    @PostMapping("/new")
    public String studentNew(@RequestBody Student student) {

        service.saveStudent(student);
        return "+Student";
    }
    @PostMapping("/take")
    public Student studentTake(@RequestParam Integer id) {
        return service.getStudentById(id).get();
    }
}